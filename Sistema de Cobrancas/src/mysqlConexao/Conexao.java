package mysqlConexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Conexao {

    public Statement stm;
    public ResultSet rs;
    public Connection conn;

    public Connection getConnection() { //metodo para abrir a conexao
        try {
            String driver = "com.mysql.jdbc.Driver"; //driver obrigatorio para a inicializacao do banco
            String url = "jdbc:mysql://localhost:3306/database"; //url de conexao
            String username = "root"; //login do banco
            String password = "123adr"; //senha do banco.
            Class.forName(driver); //Inicializa o driver para permitir uma conexao

            conn = DriverManager.getConnection(url, username, password); //usa a url, login e senha para conectar
        } catch (SQLException | ClassNotFoundException e) {// tratamento de erro
            JOptionPane.showMessageDialog(null, "Erro ao conectar com o banco de dados\n\n"+e, "Falha na Conexao", JOptionPane.OK_OPTION);
        }
        return conn;
    }

    public void fecharConexao() { //auto explicativo jovem mancebo.
        try {
            conn.close();
        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }
    }
    //Metodo que tem como parametro uma Query SQL recebida por um argumento na chamada
    public void executaQuerySQL(String sql) {
        try {
            //conn = getConnection(); //Abro a conexao para permitir a manipulacao do banco de dados.
            stm = conn.createStatement(); //criacao do objeto que executa instrucoes no banco de dados.
            rs = stm.executeQuery(sql); //executo a query armazenada em "sql" trazida por argumento na chamada.
            //stm.close();
        } catch (Exception e) {
            //System.out.println("Falha ao executar a query!!!  --  "+e); //tratamento de erro caso nao funcione a query.
            JOptionPane.showMessageDialog(null, "Falha ao executar a Query!!!\n\n"+e, "Falha no MYSQL", JOptionPane.OK_OPTION);
        }
    }
}
