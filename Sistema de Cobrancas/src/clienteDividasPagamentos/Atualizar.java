/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteDividasPagamentos;

import java.sql.*;
import mysqlConexao.Conexao;
import java.util.Date;

/**
 *
 * @author adria
 */
public class Atualizar extends Conexao{
    
    public void atualizarCliente(int idCliente, String nomeCliente, String endereco, String estado, long telefone, String cpf, String email) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("UPDATE cliente SET nomeCliente=?, endereco=?, uf=?, telefone= ? , cpf= ?, email=? WHERE idClient = ?");
        
       // int id = Integer.parseInt(idCliente);
        
        create.setString(1, nomeCliente);
        create.setString(2, endereco);
        create.setString(3, estado);
        create.setLong(4, telefone);
        create.setString(5, cpf);
        create.setString(6, email);
        create.setInt(7, idCliente);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
    
    public void atualizarDivida(int codigo, String credor, String devedor, Date dataAtualizacao, float valorDivida, String cpf) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("UPDATE divida SET credor=?, devedor=?, dataAtualizacao=?, valorDivida= ?, cpf= ? WHERE codigo = ?");
        
       // int id = Integer.parseInt(idCliente);
        
        create.setString(1, credor);
        create.setString(2, devedor);
        create.setDate(3, new java.sql.Date(dataAtualizacao.getTime()));
        create.setFloat(4, valorDivida);
        create.setString(5, cpf);
        create.setInt(6, codigo);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
}
