/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteDividasPagamentos;

import design.CadastroClientes;
import design.CadastroDividas;
import design.CadastroPagamentos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import model.ModeloTabela;
import mysqlConexao.Conexao;

/**
 *
 * @author AngeL
 */
public class Pesquisar {

    Conexao conex = new Conexao();

    public void pesquisarNomeCliente(String Sql) {
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"ID", "Nome", "Endereco", "Estado", "Telefone", "CPF", "E-Mail"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            conex.executaQuerySQL(Sql);

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            conex.rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{conex.rs.getInt("idClient"), conex.rs.getString("nomeCliente"), conex.rs.getString("endereco"), conex.rs.getString("uf"), conex.rs.getBigDecimal("telefone"), conex.rs.getString("cpf"), conex.rs.getString("email")});
            } while (conex.rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            conex.stm.close();
            //System.out.println("Tabela preenchida com sucesso!");
        } catch (SQLException e) {
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu jTable1, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        CadastroClientes.jTable1.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //ID
        CadastroClientes.jTable1.getColumnModel().getColumn(0).setPreferredWidth(0);
        CadastroClientes.jTable1.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //Nome
        CadastroClientes.jTable1.getColumnModel().getColumn(1).setPreferredWidth(170);
        CadastroClientes.jTable1.getColumnModel().getColumn(1).setResizable(true); //as seguintes eu permito isso.
        //Endereco
        CadastroClientes.jTable1.getColumnModel().getColumn(2).setPreferredWidth(140);
        CadastroClientes.jTable1.getColumnModel().getColumn(2).setResizable(true);
        //Estado
        CadastroClientes.jTable1.getColumnModel().getColumn(3).setPreferredWidth(50);
        CadastroClientes.jTable1.getColumnModel().getColumn(3).setResizable(true);
        //Telefone
        CadastroClientes.jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
        CadastroClientes.jTable1.getColumnModel().getColumn(4).setResizable(true);
        //CPF
        CadastroClientes.jTable1.getColumnModel().getColumn(5).setPreferredWidth(90);
        CadastroClientes.jTable1.getColumnModel().getColumn(5).setResizable(true);
        //RG
        //jTable1.getColumnModel().getColumn(6).setPreferredWidth(100);
        //jTable1.getColumnModel().getColumn(6).setResizable(true);
        //E-Mail
        CadastroClientes.jTable1.getColumnModel().getColumn(6).setPreferredWidth(0);
        CadastroClientes.jTable1.getColumnModel().getColumn(6).setResizable(true);

        //nao pode alterar a ordem
        CadastroClientes.jTable1.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        CadastroClientes.jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        CadastroClientes.jTable1.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        conex.fecharConexao(); //fecho a conexao depois de tudo.
    }

    public void pesquisarDividaCPF(String Sql) {
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"ID", "Credor", "Data", "Valor R$", "Devedor", "CPF", "PAGO"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            conex.executaQuerySQL(Sql);

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            conex.rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{conex.rs.getInt("codigo"), conex.rs.getString("credor"), conex.rs.getString("dataAtualizacao"), conex.rs.getFloat("valorDivida"), conex.rs.getString("devedor"), conex.rs.getString("cpf"), conex.rs.getString("pago")});
            } while (conex.rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            conex.stm.close();
            conex.rs.close();
        } catch (SQLException e) {
            //Exibe uma caixa de Mensagem com erro
            try {

            } catch (Exception s) {
            }
            //JOptionPane.showMessageDialog(null, "Erro ao preencher a tabela", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu CadastroPagamentos.jtableFaturamento, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        CadastroPagamentos.jtableFaturamento.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //Codigo
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(0).setPreferredWidth(0);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //Credor
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(1).setPreferredWidth(160);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(1).setResizable(true); //as seguintes eu permito isso.
        //Data Atualizacao
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(2).setPreferredWidth(80);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(2).setResizable(true);
        //Valor da Divida
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(3).setPreferredWidth(60);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(3).setResizable(true);
        //Devedor
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(4).setPreferredWidth(160);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(4).setResizable(true);
        //CPF
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(5).setPreferredWidth(90);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(5).setResizable(true);
        //Pago
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(6).setPreferredWidth(50);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(6).setResizable(true);

        //nao pode alterar a ordem
        CadastroPagamentos.jtableFaturamento.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        CadastroPagamentos.jtableFaturamento.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        CadastroPagamentos.jtableFaturamento.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        conex.fecharConexao(); //fecho a conexao depois de tudo.
    }

    public void pesquisarFaturamento(Date data1, Date data2) {
        Connection conn = conex.getConnection();
        ResultSet rs = null;
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"ID", "Data Pagamento", "Divida R$", "Valor Pago R$", "Forma Pagamento", "Pagante"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            //conex.executaQuerySQL(Sql);
            PreparedStatement create = conn.prepareStatement("SELECT * FROM pagamento WHERE dataPagamento >= ? AND dataPagamento <= ?");
            create.setDate(1, new java.sql.Date(data1.getTime()));
            create.setDate(2, new java.sql.Date(data2.getTime()));
            
            rs = create.executeQuery();

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{rs.getInt("idpag"), rs.getString("dataPagamento"), rs.getFloat("valorDivida"), rs.getFloat("valorPago"), rs.getString("formaPagamento"), rs.getString("pagante")});
            } while (rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            //stm.close();
            rs.close();
        } catch (SQLException e) {
            //Exibe uma caixa de Mensagem com erro
            try {

            } catch (Exception s) {
            }
            //JOptionPane.showMessageDialog(null, "Erro ao preencher a tabela", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu CadastroPagamentos.jtableFaturamento, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        CadastroPagamentos.jtableFaturamento.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //Codigo
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(0).setPreferredWidth(0);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //Credor
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(1).setPreferredWidth(160);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(1).setResizable(true); //as seguintes eu permito isso.
        //Data Atualizacao
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(2).setPreferredWidth(80);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(2).setResizable(true);
        //Valor da Divida
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(3).setPreferredWidth(60);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(3).setResizable(true);
        //Devedor
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(4).setPreferredWidth(160);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(4).setResizable(true);
        //CPF
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(5).setPreferredWidth(90);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(5).setResizable(true);
        //Pago
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(6).setPreferredWidth(50);
        CadastroPagamentos.jtableFaturamento.getColumnModel().getColumn(6).setResizable(true);

        //nao pode alterar a ordem
        CadastroPagamentos.jtableFaturamento.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        CadastroPagamentos.jtableFaturamento.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        CadastroPagamentos.jtableFaturamento.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        try {
            conn.close(); //fecho a conexao depois de tudo.
        } catch (SQLException ex) {
            Logger.getLogger(Pesquisar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
