/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteDividasPagamentos;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import mysqlConexao.Conexao;

/**
 *
 * @author adria
 */
public class Deletar extends Conexao{
    public void deletarCliente(int idCliente) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("DELETE FROM cliente WHERE idClient = ?");
        
        create.setInt(1, idCliente);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
    
    public void deletarDivida(int idDivida) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("DELETE FROM divida WHERE codigo = ?");
        
        create.setInt(1, idDivida);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
    
    public void deletarPagamento(int idPagamento) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("DELETE FROM pagamento where idpag = ?");
        
        create.setInt(1, idPagamento);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
}
