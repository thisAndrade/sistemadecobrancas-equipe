package clienteDividasPagamentos;

import mysqlConexao.Conexao;
import java.sql.PreparedStatement;
import java.util.Date;

public class Cadastrar extends Conexao {

    public void cadastrarCliente(String nomeCliente, String enderecoCliente, String estado, long telefone, String cpf, String email) throws Exception {

        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("INSERT INTO cliente (nomeCliente, endereco, uf, telefone, cpf, email) VALUES (?, ?, ?, ?, ?, ?)");
        create.setString(1, nomeCliente);
        create.setString(2, enderecoCliente);
        create.setString(3, estado);
        create.setLong(4, telefone);
        create.setString(5, cpf);
        create.setString(6, email);
        create.executeUpdate();
        create.close();
        //System.out.println("Dados inseridos com Sucesso!");
        fecharConexao();
    }

    public void cadastrarDivida(String credor, Date dataAtualizacao, float valorDivida, String devedor, String cpf, String pago) throws Exception {
        conn = getConnection();
        //PreparedStatement create = conn.prepareStatement("INSERT INTO divida (credor, dataAtualizacao, valorDivida, devedor) VALUES ('" + credor + "', '" + dataAtualizacao + "'," + valorDivida + ",'" + devedor + "')");
        
        PreparedStatement create = conn.prepareStatement("INSERT INTO divida (credor, dataAtualizacao, valorDivida, devedor, cpf, pago) VALUES (?, ?, ?, ?, ?, ?)");
        create.setString(1, credor);
        create.setDate(2, new java.sql.Date(dataAtualizacao.getTime()));
        create.setFloat(3, valorDivida);
        create.setString(4, devedor);
        create.setString(5, cpf);
        create.setString(6, pago);
        
        create.executeUpdate();
        create.close();
        //System.out.println("Dados inseridos com Sucesso!");
        fecharConexao();
    }
    
    public void cadastrarPagamento(int idDivida, Date dataPagamento, float valorPago, String formaPagamento, String pagante, float valorDivida) throws Exception{
        conn = getConnection();
        
        PreparedStatement create = conn.prepareStatement("INSERT INTO pagamento (divida, dataPagamento, valorPago, formaPagamento, pagante, valorDivida) VALUES (?, ?, ?, ?, ?, ?)");
        create.setInt(1, idDivida);
        create.setDate(2, new java.sql.Date(dataPagamento.getTime()));
        create.setFloat(3, valorPago);
        create.setString(4, formaPagamento);
        create.setString(5, pagante);
        create.setFloat(6, valorDivida);
        
        create.executeUpdate();
        create.close();
        fecharConexao();
    
    
    }
}
