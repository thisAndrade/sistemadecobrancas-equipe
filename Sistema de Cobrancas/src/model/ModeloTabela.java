        /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import design.CadastroDividas;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author adria
 */
public class ModeloTabela extends AbstractTableModel {

    private ArrayList linhas = null; //Armazenamos as linhas em um ArrayList (uma colecao de objetos, dados).
    private String[] colunas = null; //Variavel para armazenas as colunas, cada coluna fica em um vetor "colunas[0]..."
        
    //Metodo Construtor que recebe uma lista de linhas e uma sequencia de colunas.
    public ModeloTabela(ArrayList linhas, String[] colunas) {
        setLinhas(linhas); //Chama o "set" de linhas para atribuir a quantidade de linhas na variavel "linhas".
        setColunas(colunas); //O mesmo de cima porem com a quantidade de colunas

    }

    //Pega a quantidade de linhas, a variavel esta privada entao somente esta classe tem acesso
    //por meio desse metodo "get".
    public ArrayList getLinhas() {
        return linhas;
    }

    //Recebe a quantidade de linhas e "seta" na variavel String "linhas"
    private void setLinhas(ArrayList linhas) {
        this.linhas = linhas;
    }

    //Pega a quantidade de colunas que vai ser armazenada na variavel no "set" abaixo.
    public String[] getColunas() {
        return colunas;
    }

    //Recebe a quantidade de colunas e "seta" na variavel String "colunas"
    private void setColunas(String[] colunas) {
        this.colunas = colunas;
    }

    //Pega o tamanho das colunas, a variavel coluna e um vetor de String logo
    //usando o length voce pega o tamanho do vetor e se cada espaco e uma coluna
    //logo o tamanho dele e a quantidade de colunas.
    @Override
    public int getColumnCount() {
        return colunas.length;
        //contar o numero de colunas
        //length = quantidade do numero de colunas
    }

    //Mesma logica das colunas acima, aqui eu pego o tamanho do vetor de linhas
    //se tem 10 espacos, logo tem o tamanho 10, logo eu sei que tem 10 linhas :D
    @Override
    public int getRowCount() {
        return linhas.size();
        //contagem de quantas linhas tem no nosso array
    }
    
    //Pego o nome da coluna de acordo com o numero da coluna
    @Override
    public String getColumnName(int numCol) {
        return colunas[numCol];
        //retorna o valor da coluna(logicamente seu nome).
        //pegar o valor do nome da coluna, logo pegar o nome da coluna.
    }

    //Meotodo para sobreescrever o metodo da interface "TableModel" onde aqui ele pega
    //a linha e a coluna do valor pedido, como se fosse batalha naval, por exemplo: "Coluna A, Linha 4" 
    @Override
    public Object getValueAt(int numLin, int numCol) {
        //Crio uma variavel chamada "linha" do tipo "Objeto" onde ela armazena um Cast(conversao) para "Object" da linha em questao.
        Object[] linha = (Object[]) getLinhas().get(numLin);//o "get" pega toda a linha.
        return linha[numCol];//retorno toda a linha na coluna pedida.
    }
}
