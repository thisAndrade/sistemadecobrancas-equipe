package clienteDividasPagamentos;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Date;
import mysqlConexao.Conexao;

/**
 *
 * @author adria
 */
public class Atualizar extends Conexao {

    public void atualizarCliente(int idCliente, String nomeCliente, String endereco, String estado, String telefone, String cpf, String email) throws SQLException {

        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("UPDATE tb_pessoa SET pe_nome =?, pe_endereco=?, pe_uf=?, pe_telefone= ? , pe_documento= ?, pe_email=? WHERE pe_codigo = ?");

       // int id = Integer.parseInt(idCliente);
        create.setString(1, nomeCliente);
        create.setString(2, endereco);
        create.setString(3, estado);
        create.setString(4, telefone);
        create.setString(5, cpf);
        create.setString(6, email);
        create.setInt(7, idCliente);

        create.executeUpdate();

        create.close();
        fecharConexao();
    }
    
    public void atualizarDivida(int codigo, int credor, int devedor, Date dataAtualizacao, float valorDivida) throws Exception{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("UPDATE tb_divida SET div_credor=?, div_devedor=?, div_dataatualizacao=?, div_valordadivida= ? WHERE div_codigo = ?");
        
        create.setInt(1, credor);
        create.setInt(2, devedor);
        create.setDate(3, new java.sql.Date(dataAtualizacao.getTime()));
        create.setFloat(4, valorDivida);
        create.setInt(5, codigo);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
}
