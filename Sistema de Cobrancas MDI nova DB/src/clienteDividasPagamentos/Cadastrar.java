
package clienteDividasPagamentos;

import java.math.BigDecimal;
import mysqlConexao.Conexao;
import java.sql.PreparedStatement;
import java.util.Date;

public class Cadastrar extends Conexao {

    public void cadastrarCliente(String nomeCliente, String enderecoCliente, String estado, String telefone, String cpf, String email) throws Exception {

        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("INSERT INTO tb_pessoa (pe_nome, pe_endereco, pe_uf, pe_telefone, pe_documento, pe_email) VALUES (?, ?, ?, ?, ?, ?)");
        create.setString(1, nomeCliente);
        create.setString(2, enderecoCliente);
        create.setString(3, estado);
        create.setString(4, telefone);
        create.setString(5, cpf);
        create.setString(6, email);
        create.executeUpdate();
        create.close();
        //System.out.println("Dados inseridos com Sucesso!");
        fecharConexao();
    }
    
    public void cadastrarDivida(int credor, int devedor, Date dataAtualizacao, float div_valordadivida, String pago) throws Exception {
        conn = getConnection();
        //PreparedStatement create = conn.prepareStatement("INSERT INTO divida (credor, dataAtualizacao, valorDivida, devedor) VALUES ('" + credor + "', '" + dataAtualizacao + "'," + valorDivida + ",'" + devedor + "')");
        
        PreparedStatement create = conn.prepareStatement("INSERT INTO tb_divida (div_credor, div_devedor, div_dataatualizacao, div_valordadivida, div_pago) VALUES (?, ?, ?, ?, ?)");
        create.setInt(1, credor);
        create.setInt(2, devedor);
        create.setDate(3, new java.sql.Date(dataAtualizacao.getTime()));
        create.setFloat(4, div_valordadivida);
        create.setString(5, pago);
        
        create.executeUpdate();
        create.close();
        //System.out.println("Dados inseridos com Sucesso!");
        fecharConexao();
    }
    
    public void cadastrarPagamento(int idDivida, Date dataPagamento, float valorPago, String formaPagamento) throws Exception{
        conn = getConnection();
        
        PreparedStatement create = conn.prepareStatement("INSERT INTO tb_pagamento (pag_div_codigo, pag_datapagamento, pag_valorpago, pag_forma) VALUES (?, ?, ?, ?)");
        create.setInt(1, idDivida);
        create.setDate(2, new java.sql.Date(dataPagamento.getTime()));
        create.setFloat(3, valorPago);
        create.setString(4, formaPagamento);
        
        create.executeUpdate();
        create.close();
        fecharConexao();
    
    
    }
}

