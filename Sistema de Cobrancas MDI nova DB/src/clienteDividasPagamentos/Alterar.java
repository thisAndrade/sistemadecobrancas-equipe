/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteDividasPagamentos;

import java.sql.PreparedStatement;
import mysqlConexao.Conexao;

/**
 *
 * @author adria
 */
public class Alterar extends Conexao{
    public void alterarStatusDivida(int idDivida, String resposta) throws Exception{
        conn = getConnection();
        
        PreparedStatement create = conn.prepareStatement("UPDATE tb_divida SET div_pago = ? WHERE div_codigo = ?");
        create.setString(1, resposta);
        create.setInt(2, idDivida);
        
        create.executeUpdate();
        create.close();
        fecharConexao();
    }
}
