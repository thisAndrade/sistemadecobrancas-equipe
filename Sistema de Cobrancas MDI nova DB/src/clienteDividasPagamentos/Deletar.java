package clienteDividasPagamentos;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import mysqlConexao.Conexao;

public class Deletar extends Conexao{
    public void deletarCliente(int idCliente) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("DELETE FROM tb_pessoa WHERE pe_codigo = ?");
        
        create.setInt(1, idCliente);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
    
    public void deletarDivida(int idDivida) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("DELETE FROM tb_divida WHERE div_codigo = ?");
        
        create.setInt(1, idDivida);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
    
    public void deletarPagamento(int idPagamento) throws SQLException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("DELETE FROM tb_pagamento where pag_codigo = ?");
        
        create.setInt(1, idPagamento);
        
        create.executeUpdate();
        
        create.close();
        fecharConexao();
    }
}
