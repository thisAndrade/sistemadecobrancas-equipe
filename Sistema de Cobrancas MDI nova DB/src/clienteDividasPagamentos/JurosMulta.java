/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteDividasPagamentos;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import mysqlConexao.Conexao;

/**
 *
 * @author adria
 */
public class JurosMulta extends Conexao{
    Connection conn = null;
    ResultSet rs = null;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

    public BigDecimal calcularJuros(Date dataDivida, Date dataPagamento, String valorDivida) {
        BigDecimal valorFixo = new BigDecimal(valorDivida);
        BigDecimal juros = new BigDecimal(0);
        BigDecimal cem = new BigDecimal(100);
        
        try {
            conn = getConnection();

            PreparedStatement create = conn.prepareStatement("SELECT TIMESTAMPDIFF(DAY,?,?)");
            create.setDate(1, new java.sql.Date(dataDivida.getTime()));
            create.setDate(2, new java.sql.Date(dataPagamento.getTime()));

            rs = create.executeQuery();
            rs.first();
            int dias = Integer.parseInt(rs.getObject(1).toString());

            for (int i = 1; i <= dias; i++) {
                BigDecimal res = new BigDecimal(0.35).divide(cem).multiply(valorFixo);
                juros = juros.add(res).setScale(2, RoundingMode.UP);
            }
            valorFixo = valorFixo.add(juros);
            rs.close();
            fecharConexao();

        } catch (SQLException | NumberFormatException e) {
        }
        //System.out.println("Valor com JUROS : "+valorFixo);
        return juros;
    }
    
    public BigDecimal calcularMulta(String valorDividaString) {
        BigDecimal valorDivida = new BigDecimal(valorDividaString);
        BigDecimal res =  new BigDecimal(0);
        BigDecimal cem = new BigDecimal(100);
        BigDecimal multa = new BigDecimal(2);
        res = valorDivida.divide(cem).multiply(multa);
        valorDivida = res.add(valorDivida).setScale(2, RoundingMode.UP);
        
        //System.out.println("\nValor Multado : "+valorDivida);
        return valorDivida;
    }
}
