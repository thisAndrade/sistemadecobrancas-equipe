package clienteDividasPagamentos;

import design.CadastroClientes;
import design.CadastroDividas;
import design.CadastroPagamentos;
import design.ConsultarFaturamento;
import java.math.BigDecimal;
//import design.CadastroPagamentos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import model.ModeloTabela;
import mysqlConexao.Conexao;

/**
 *
 * @author AngeL
 */
public class Pesquisar {

    Conexao conex = new Conexao();

    public void pesquisarNomeCliente(String Sql) {
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"ID", "Nome", "Endereco", "Estado", "Telefone", "CPF", "E-Mail"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            conex.executaQuerySQL(Sql);

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            conex.rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{conex.rs.getInt("pe_codigo"), conex.rs.getString("pe_nome"), conex.rs.getString("pe_endereco"), conex.rs.getString("pe_uf"), conex.rs.getBigDecimal("pe_telefone"), conex.rs.getString("pe_documento"), conex.rs.getString("pe_email")});
            } while (conex.rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            conex.stm.close();
            //System.out.println("Tabela preenchida com sucesso!");
        } catch (SQLException e) {
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu jTable1, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        CadastroClientes.jTable1.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //ID
        CadastroClientes.jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
        CadastroClientes.jTable1.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //Nome
        CadastroClientes.jTable1.getColumnModel().getColumn(1).setPreferredWidth(170);
        CadastroClientes.jTable1.getColumnModel().getColumn(1).setResizable(true); //as seguintes eu permito isso.
        //Endereco
        CadastroClientes.jTable1.getColumnModel().getColumn(2).setPreferredWidth(140);
        CadastroClientes.jTable1.getColumnModel().getColumn(2).setResizable(true);
        //Estado
        CadastroClientes.jTable1.getColumnModel().getColumn(3).setPreferredWidth(64);
        CadastroClientes.jTable1.getColumnModel().getColumn(3).setResizable(true);
        //Telefone
        CadastroClientes.jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
        CadastroClientes.jTable1.getColumnModel().getColumn(4).setResizable(true);
        //CPF
        CadastroClientes.jTable1.getColumnModel().getColumn(5).setPreferredWidth(90);
        CadastroClientes.jTable1.getColumnModel().getColumn(5).setResizable(true);
        //RG
        //jTable1.getColumnModel().getColumn(6).setPreferredWidth(100);
        //jTable1.getColumnModel().getColumn(6).setResizable(true);
        //E-Mail
        CadastroClientes.jTable1.getColumnModel().getColumn(6).setPreferredWidth(250);
        CadastroClientes.jTable1.getColumnModel().getColumn(6).setResizable(true);

        //nao pode alterar a ordem
        CadastroClientes.jTable1.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        CadastroClientes.jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        CadastroClientes.jTable1.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        conex.fecharConexao(); //fecho a conexao depois de tudo.
    }
    
    public void pesquisarDividaCPF(String Sql) {
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"ID", "div_credor", "div_devedor", "Data", "Valor R$", "Documento(CPF)","Nome", "PAGO"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            conex.executaQuerySQL(Sql);

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            conex.rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{conex.rs.getInt("div_codigo"), conex.rs.getInt("div_credor"), conex.rs.getInt("div_devedor"), conex.rs.getDate("div_dataatualizacao"), conex.rs.getBigDecimal("div_valordadivida"), conex.rs.getString("pe_documento"), conex.rs.getString("pe_nome"), conex.rs.getString("div_pago")});
            } while (conex.rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            conex.stm.close();
            conex.rs.close();
        } catch (SQLException e) {
            //Exibe uma caixa de Mensagem com erro
            if(dados.size() < 1){
                JOptionPane.showMessageDialog(null, "Nao foram encontrados resultados!", "NOT FOUND RESULTS", JOptionPane.WARNING_MESSAGE);
            }
            //JOptionPane.showMessageDialog(null, "Erro ao preencher a tabela", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu CadastroPagamentos.jtableFaturamento, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        CadastroDividas.jtableDividas.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //Codigo
        CadastroDividas.jtableDividas.getColumnModel().getColumn(0).setPreferredWidth(30);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //div_credor - deixar invisivel
        CadastroDividas.jtableDividas.getColumnModel().getColumn(1).setMaxWidth(0);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(1).setMinWidth(0);
        CadastroDividas.jtableDividas.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
        CadastroDividas.jtableDividas.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
        //div_devedor - deixar invisivel
        CadastroDividas.jtableDividas.getColumnModel().getColumn(2).setMaxWidth(0);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(2).setMinWidth(0);
        CadastroDividas.jtableDividas.getTableHeader().getColumnModel().getColumn(2).setMaxWidth(0);
        CadastroDividas.jtableDividas.getTableHeader().getColumnModel().getColumn(2).setMinWidth(0);
        //Data
        CadastroDividas.jtableDividas.getColumnModel().getColumn(3).setPreferredWidth(80);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(3).setResizable(true);
        //Valor da Divida
        CadastroDividas.jtableDividas.getColumnModel().getColumn(4).setPreferredWidth(70);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(4).setResizable(true);
        //CPF
        CadastroDividas.jtableDividas.getColumnModel().getColumn(5).setPreferredWidth(100);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(5).setResizable(true);
        //Nome
        CadastroDividas.jtableDividas.getColumnModel().getColumn(6).setPreferredWidth(200);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(6).setResizable(true);
        //Pago
        CadastroDividas.jtableDividas.getColumnModel().getColumn(7).setPreferredWidth(50);
        CadastroDividas.jtableDividas.getColumnModel().getColumn(7).setResizable(true);

        //nao pode alterar a ordem
        CadastroDividas.jtableDividas.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        CadastroDividas.jtableDividas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        CadastroDividas.jtableDividas.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        conex.fecharConexao(); //fecho a conexao depois de tudo.
    }
    
    public void pesquisarPagamentoCPF(String Sql) {
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"ID", "pag_div_codigo", "Data", "Valor R$", "Forma Pagamento", "div_codigo","div_devedor", "pe_codigo", "Nome", "Documento(CPF)"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            conex.executaQuerySQL(Sql);

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            conex.rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{conex.rs.getInt("pag_codigo"), conex.rs.getInt("pag_div_codigo"), conex.rs.getDate("pag_datapagamento"), conex.rs.getBigDecimal("pag_valorpago"), conex.rs.getString("pag_forma"), conex.rs.getInt("div_codigo"), conex.rs.getInt("div_devedor"), conex.rs.getInt("pe_codigo"), conex.rs.getString("pe_nome"), conex.rs.getString("pe_documento")});
            } while (conex.rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            conex.stm.close();
            conex.rs.close();
        } catch (SQLException e) {
            //Exibe uma caixa de Mensagem com erro
            if(dados.size() < 1){
                JOptionPane.showMessageDialog(null, "Nao foram encontrados resultados!", "NOT FOUND RESULTS", JOptionPane.WARNING_MESSAGE);
            }
            //JOptionPane.showMessageDialog(null, "Erro ao preencher a tabela", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu CadastroPagamentos.jtableFaturamento, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        CadastroPagamentos.jtablePagamentos.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //Codigo
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(0).setPreferredWidth(30);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //div_credor - deixar invisivel
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(1).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(1).setMinWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
        //div_devedor - deixar invisivel
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(2).setPreferredWidth(80);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(2).setResizable(true);
        //Data
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(3).setPreferredWidth(60);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(3).setResizable(true);
        //Valor da Divida
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(4).setPreferredWidth(110);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(4).setResizable(true);
        //CPF
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(5).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(5).setMinWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(5).setMinWidth(0);
        //Nome
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(6).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(6).setMinWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);
        //Pago
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(7).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(7).setMinWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(7).setMaxWidth(0);
        CadastroPagamentos.jtablePagamentos.getTableHeader().getColumnModel().getColumn(7).setMinWidth(0);

        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(8).setPreferredWidth(160);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(8).setResizable(true);
        
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(9).setPreferredWidth(110);
        CadastroPagamentos.jtablePagamentos.getColumnModel().getColumn(9).setResizable(true);
        
        
        //nao pode alterar a ordem
        CadastroPagamentos.jtablePagamentos.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        CadastroPagamentos.jtablePagamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        CadastroPagamentos.jtablePagamentos.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        conex.fecharConexao(); //fecho a conexao depois de tudo.
    }
    
    public int pegarIDpessoa(String nome) throws SQLException{
        
        Connection conn = conex.getConnection();
        ResultSet rs = null;
        
        PreparedStatement create = conn.prepareStatement("SELECT pe_codigo FROM tb_pessoa WHERE pe_nome = ?");
        
        create.setString(1, nome);
        
        rs = create.executeQuery();
        
        rs.first();
        
        int id = Integer.parseInt(rs.getObject(1).toString());
        
        return id;
    }
    
    public void pesquisarFaturamento(Date data1, Date data2) {
        Connection conn = conex.getConnection();
        ResultSet rs = null;
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"Data", "Valor Pago", "Nome Devedor", "Forma Pagamento"};
        float valor;
        float total = 0;
        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            //conex.executaQuerySQL(Sql);
            PreparedStatement create = conn.prepareStatement("SELECT pag_datapagamento, pag_valorpago, pe_nome, pag_forma FROM tb_pagamento INNER JOIN tb_divida ON tb_divida.div_codigo = tb_pagamento.pag_div_codigo "
                    + "INNER JOIN tb_pessoa ON tb_pessoa.pe_codigo = tb_divida.div_devedor WHERE pag_datapagamento >= ? AND pag_datapagamento <= ?");
            create.setDate(1, new java.sql.Date(data1.getTime()));
            create.setDate(2, new java.sql.Date(data2.getTime()));
            
            rs = create.executeQuery();

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{rs.getDate("pag_datapagamento"), rs.getBigDecimal("pag_valorpago"), rs.getString("pe_nome"), rs.getString("pag_forma")});
                valor = rs.getBigDecimal("pag_valorpago").floatValue();
                total = total + valor;
            } while (rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            //stm.close();
            rs.close();
        } catch (SQLException e) {
            //Exibe uma caixa de Mensagem com erro
            try {

            } catch (Exception s) {
            }
            //JOptionPane.showMessageDialog(null, "Erro ao preencher a tabela", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu CadastroPagamentos.jtableFaturamento, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        ConsultarFaturamento.jtableFaturamento.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //Data
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(0).setPreferredWidth(80);
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //Valor pago
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(1).setPreferredWidth(80);
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(1).setResizable(true); //as seguintes eu permito isso.
        //Nome
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(2).setPreferredWidth(160);
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(2).setResizable(true);
        //Forma de Pagamento
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(3).setPreferredWidth(130);
        ConsultarFaturamento.jtableFaturamento.getColumnModel().getColumn(3).setResizable(true);

        //nao pode alterar a ordem
        ConsultarFaturamento.jtableFaturamento.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        ConsultarFaturamento.jtableFaturamento.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        ConsultarFaturamento.jtableFaturamento.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        ConsultarFaturamento.lblTotal.setText("R$: "+String.valueOf(total));
        try {
            conn.close(); //fecho a conexao depois de tudo.
        } catch (SQLException ex) {
            Logger.getLogger(Pesquisar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
