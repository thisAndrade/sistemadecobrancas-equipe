package design;

import clienteDividasPagamentos.Atualizar;
import clienteDividasPagamentos.Cadastrar;
import clienteDividasPagamentos.Deletar;
import clienteDividasPagamentos.Pesquisar;
import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import model.ModeloTabela;
import mysqlConexao.Conexao;

public class CadastroClientes extends javax.swing.JFrame {

    Conexao conex = new Conexao();
    public String selectClienteDBquery = "select * from tb_pessoa";

    int mouseX;
    int mouseY;

    public CadastroClientes() {
        initComponents();
        preencherTabelaClientes(selectClienteDBquery);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtPesquisarCliente = new javax.swing.JTextField();
        txtNomeCliente = new javax.swing.JTextField();
        txtEndereco = new javax.swing.JTextField();
        txtTelefone = new javax.swing.JTextField();
        txtEstado = new javax.swing.JTextField();
        txtCPF = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        btnExcluir = new javax.swing.JButton();
        btnInserir = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1280, 720));
        setPreferredSize(new java.awt.Dimension(1280, 720));
        setResizable(false);
        setSize(new java.awt.Dimension(1280, 720));
        getContentPane().setLayout(null);

        jDesktopPane1.setBackground(new java.awt.Color(240, 240, 240));
        jDesktopPane1.setMinimumSize(new java.awt.Dimension(1280, 720));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("Busca : ");
        jDesktopPane1.add(jLabel1);
        jLabel1.setBounds(280, 290, 60, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 204, 204));
        jLabel2.setText("Nome :");
        jDesktopPane1.add(jLabel2);
        jLabel2.setBounds(280, 100, 270, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(204, 204, 204));
        jLabel3.setText("Endereco : ");
        jDesktopPane1.add(jLabel3);
        jLabel3.setBounds(590, 100, 90, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 204, 204));
        jLabel4.setText("Telefone : ");
        jDesktopPane1.add(jLabel4);
        jLabel4.setBounds(280, 160, 100, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 204, 204));
        jLabel5.setText("Estado : ");
        jDesktopPane1.add(jLabel5);
        jLabel5.setBounds(590, 160, 70, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(204, 204, 204));
        jLabel6.setText("Documento(CPF) :");
        jDesktopPane1.add(jLabel6);
        jLabel6.setBounds(280, 220, 140, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(204, 204, 204));
        jLabel7.setText("E-Mail : ");
        jDesktopPane1.add(jLabel7);
        jLabel7.setBounds(590, 220, 70, 30);

        txtPesquisarCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisarClienteKeyPressed(evt);
            }
        });
        jDesktopPane1.add(txtPesquisarCliente);
        txtPesquisarCliente.setBounds(340, 290, 620, 30);
        jDesktopPane1.add(txtNomeCliente);
        txtNomeCliente.setBounds(280, 130, 230, 30);
        jDesktopPane1.add(txtEndereco);
        txtEndereco.setBounds(590, 130, 230, 30);
        jDesktopPane1.add(txtTelefone);
        txtTelefone.setBounds(280, 190, 230, 30);
        jDesktopPane1.add(txtEstado);
        txtEstado.setBounds(590, 190, 230, 30);

        txtCPF.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCPFKeyTyped(evt);
            }
        });
        jDesktopPane1.add(txtCPF);
        txtCPF.setBounds(280, 250, 230, 30);
        jDesktopPane1.add(txtEmail);
        txtEmail.setBounds(590, 250, 230, 30);

        btnExcluir.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnExcluir.setForeground(new java.awt.Color(204, 0, 0));
        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_remove_user_male_25px.png"))); // NOI18N
        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        jDesktopPane1.add(btnExcluir);
        btnExcluir.setBounds(830, 250, 130, 30);

        btnInserir.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnInserir.setForeground(new java.awt.Color(0, 153, 102));
        btnInserir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_user_male_25px_1.png"))); // NOI18N
        btnInserir.setText("Inserir");
        btnInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInserirActionPerformed(evt);
            }
        });
        jDesktopPane1.add(btnInserir);
        btnInserir.setBounds(830, 130, 130, 30);

        btnAlterar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnAlterar.setForeground(new java.awt.Color(51, 0, 204));
        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_edit_user_male_25px.png"))); // NOI18N
        btnAlterar.setText("Alterar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        jDesktopPane1.add(btnAlterar);
        btnAlterar.setBounds(830, 190, 130, 30);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jDesktopPane1.add(jScrollPane1);
        jScrollPane1.setBounds(280, 330, 680, 250);

        getContentPane().add(jDesktopPane1);
        jDesktopPane1.setBounds(0, -20, 1280, 720);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_administrator_male_15px.png"))); // NOI18N
        jMenu1.setText("Menu");

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_user_male_20px.png"))); // NOI18N
        jMenuItem1.setText("Cadastro de Dividas");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_banknotes_20px.png"))); // NOI18N
        jMenuItem2.setText("Cadastro de Pagamentos");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_sales_performance_20px.png"))); // NOI18N
        jMenuItem3.setText("Consultar Faturamento");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public void preencherTabelaClientes(String Sql) {
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        //String[] colunas = new String[]{"ID", "Nome", "Endereco", "Estado", "Telefone", "CPF", "RG", "E-Mail"};
        String[] colunas = new String[]{"ID", "Nome", "Endereco", "Estado", "Telefone", "CPF", "E-Mail"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            conex.executaQuerySQL(Sql);

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            conex.rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                //dados.add(new Object[]{conex.rs.getInt("idClient"), conex.rs.getString("nomeCliente"), conex.rs.getString("endereco"), conex.rs.getString("uf"), conex.rs.getBigDecimal("telefone"), conex.rs.getString("cpf"), conex.rs.getString("rg"), conex.rs.getString("email")});
                dados.add(new Object[]{conex.rs.getInt("pe_codigo"), conex.rs.getString("pe_nome"), conex.rs.getString("pe_endereco"), conex.rs.getString("pe_uf"), conex.rs.getString("pe_telefone"), conex.rs.getString("pe_documento"), conex.rs.getString("pe_email")});
            } while (conex.rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            conex.stm.close();
            //System.out.println("Tabela preenchida com sucesso!");
        } catch (SQLException e) {

            try {
                if (conex.rs.next() == false) {
                    JOptionPane.showMessageDialog(this, "A Tabela 'cliente' esta sem dados para preencher a lista!", "ERRO", JOptionPane.OK_OPTION);
                } else {
                    JOptionPane.showMessageDialog(this, "Erro ao preencher a lista, verifique se o banco de dados esta online", "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception s) {
            }

            //Exibe uma caixa de Mensagem com erro
            //JOptionPane.showMessageDialog(null, "Erro ao preencher a tabela", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu jTable1, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        jTable1.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //ID
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
        jTable1.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //Nome
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(170);
        jTable1.getColumnModel().getColumn(1).setResizable(true); //as seguintes eu permito isso.
        //Endereco
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(140);
        jTable1.getColumnModel().getColumn(2).setResizable(true);
        //Estado
        jTable1.getColumnModel().getColumn(3).setPreferredWidth(64);
        jTable1.getColumnModel().getColumn(3).setResizable(true);
        //Telefone
        jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
        jTable1.getColumnModel().getColumn(4).setResizable(true);
        //CPF
        jTable1.getColumnModel().getColumn(5).setPreferredWidth(90);
        jTable1.getColumnModel().getColumn(5).setResizable(true);
        //RG
        //jTable1.getColumnModel().getColumn(6).setPreferredWidth(100);
        //jTable1.getColumnModel().getColumn(6).setResizable(true);
        //E-Mail
        jTable1.getColumnModel().getColumn(6).setPreferredWidth(250);
        jTable1.getColumnModel().getColumn(6).setResizable(true);

        //nao pode alterar a ordem
        jTable1.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        jTable1.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        conex.fecharConexao(); //fecho a conexao depois de tudo.
    }

    private void btnInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInserirActionPerformed
        Cadastrar cad = new Cadastrar();
        if (txtNomeCliente.getText().isEmpty() || txtEndereco.getText().isEmpty() || txtTelefone.getText().isEmpty() || txtCPF.getText().isEmpty() | txtEstado.getText().isEmpty() | txtEmail.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha todos os campos!!!", "CAMPOS VAZIOS", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                cad.cadastrarCliente(txtNomeCliente.getText(), txtEndereco.getText(), txtEstado.getText(), txtTelefone.getText(), txtCPF.getText(), txtEmail.getText());
                preencherTabelaClientes(selectClienteDBquery);
            } catch (Exception e) {
                //e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Erro ao cadastrar o cliente!\n\n" + e, "ERRO CADASTRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnInserirActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int linha = jTable1.getSelectedRow();
        txtNomeCliente.setText(jTable1.getValueAt(linha, 1).toString());
        txtEndereco.setText(jTable1.getValueAt(linha, 2).toString());
        txtEstado.setText(jTable1.getValueAt(linha, 3).toString());
        txtTelefone.setText(jTable1.getValueAt(linha, 4).toString());
        txtCPF.setText(jTable1.getValueAt(linha, 5).toString());
        txtEmail.setText(jTable1.getValueAt(linha, 6).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        Atualizar att = new Atualizar();
        int linha = jTable1.getSelectedRow();
        int id = Integer.parseInt(jTable1.getValueAt(linha, 0).toString());
        try {
            att.atualizarCliente(id, txtNomeCliente.getText(), txtEndereco.getText(), txtEstado.getText(), txtTelefone.getText(), txtCPF.getText(), txtEmail.getText());
            preencherTabelaClientes(selectClienteDBquery);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, "Erro ao atualizar o cadastro\n\n" + e, "ERRO ATUALIZACAO", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        Deletar delete = new Deletar();
        int linha = jTable1.getSelectedRow();
        int idClient = Integer.parseInt(jTable1.getValueAt(linha, 0).toString());
        int quantasDividas = verificarDividas(idClient);
        if (quantasDividas >= 1) {
            JOptionPane.showMessageDialog(this, "Nao foi possivel apagar o cliente pois ele possui " + quantasDividas + " divida(s), PAGUE A(S) DIVIDA(S)!", "DIVIDA(S) NAO PAGA(S)", JOptionPane.OK_OPTION);
            preencherTabelaClientes(selectClienteDBquery);
        } else {
            int retorno = decisaoDeletar("Deseja mesmo deletar esse cliente?");
            if (retorno == 0) {
                try {
                    delete.deletarCliente(idClient);
                    preencherTabelaClientes(selectClienteDBquery);
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, "Erro ao deletar o Cliente.\n\n" + e, "ERRO AO APAGAR", JOptionPane.OK_OPTION);
                }
            }
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    public int verificarDividas(int idDevedor) {
        Connection conn = null;
        ResultSet rs = null;
        int quantiaDividas = 0;
        try {
            conn = conex.getConnection();
            PreparedStatement create = conn.prepareStatement("SELECT COUNT(*) FROM tb_divida where div_devedor = ?");
            create.setInt(1, idDevedor);
            rs = create.executeQuery();
            rs.first();

            quantiaDividas = Integer.parseInt(rs.getObject(1).toString());
        } catch (SQLException | NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Erro ao verificar as Dividas.\n\n" + e, "ERRO AO VERIFICAR", JOptionPane.OK_OPTION);
        }

        return quantiaDividas;
    }

    private void txtPesquisarClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisarClienteKeyPressed
        Pesquisar pesquisa = new Pesquisar();
        if (txtPesquisarCliente.getText().length() < 1) {
            preencherTabelaClientes(selectClienteDBquery);
        } else {
            pesquisa.pesquisarNomeCliente("select * from tb_pessoa where pe_nome LIKE '%" + txtPesquisarCliente.getText() + "%'");

        }
    }//GEN-LAST:event_txtPesquisarClienteKeyPressed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        CadastroDividas cadDiv = new CadastroDividas();
        jDesktopPane1.add(cadDiv);
        cadDiv.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void txtCPFKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCPFKeyTyped
        String caracteres = "1234567890";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCPFKeyTyped

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        CadastroPagamentos cadPag = new CadastroPagamentos();
        jDesktopPane1.add(cadPag);
        cadPag.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        ConsultarFaturamento conFat = new ConsultarFaturamento();
        jDesktopPane1.add(conFat);
        conFat.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    public static int decisaoDeletar(String messagem) {
        int result = JOptionPane.showConfirmDialog((Component) null, messagem, "ALERTA", JOptionPane.OK_CANCEL_OPTION);
        return result;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroClientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnInserir;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    private javax.swing.JTextField txtCPF;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEndereco;
    private javax.swing.JTextField txtEstado;
    private javax.swing.JTextField txtNomeCliente;
    private javax.swing.JTextField txtPesquisarCliente;
    private javax.swing.JTextField txtTelefone;
    // End of variables declaration//GEN-END:variables
}
