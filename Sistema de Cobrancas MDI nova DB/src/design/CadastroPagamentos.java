/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package design;

import clienteDividasPagamentos.Alterar;
import clienteDividasPagamentos.Cadastrar;
import clienteDividasPagamentos.Deletar;
import clienteDividasPagamentos.JurosMulta;
import clienteDividasPagamentos.Pesquisar;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import model.ModeloTabela;
import mysqlConexao.Conexao;

/**
 *
 * @author adria
 */
public class CadastroPagamentos extends javax.swing.JInternalFrame {

    Conexao conex = new Conexao();
    //String pagamentosQuerySelect = "SELECT pag_codigo, pag_div_codigo, pag_datapagamento, pag_valorpago, pe_nome, pag_forma FROM tb_pagamento INNER JOIN tb_pessoa ON pag_div_codigo = tb_pessoa.pe_codigo";
    String pagamentosQuerySelect = "SELECT pag_codigo, pag_div_codigo, pag_datapagamento, pag_valorpago, pag_forma, div_codigo, div_devedor, pe_codigo, pe_nome "
            + "FROM tb_pagamento INNER JOIN tb_divida ON tb_pagamento.pag_div_codigo = tb_divida.div_codigo "
            + "INNER JOIN tb_pessoa ON tb_divida.div_devedor = tb_pessoa.pe_codigo";

    String dividasQuerySelect = "select div_codigo, div_devedor, div_dataatualizacao, div_valordadivida, div_pago, t1.pe_nome, t2.pe_nome "
            + "from tb_divida inner join tb_pessoa t1 on tb_divida.div_credor = t1.pe_codigo "
            + "inner join tb_pessoa t2 on tb_divida.div_devedor = t2.pe_codigo where div_pago = 'NAO'";
    SimpleDateFormat formatterSimple = new SimpleDateFormat("yyyy-MM-dd");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDateTime now = LocalDateTime.now();

    public CadastroPagamentos() {
        initComponents();
        preencherTableDividas(dividasQuerySelect);
        preencherTablePagamentos(pagamentosQuerySelect);
        lblTaxas.setVisible(false);
        //setVisible(true);
//        try {
//            datePagamentos.setDate(formatterSimple.parse(now.toString()));
//            System.out.println("");
//            System.out.println("");
//        } catch (ParseException ex) {
//            Logger.getLogger(CadastroPagamentos.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtValorDivida = new javax.swing.JFormattedTextField();
        txtValorPago = new javax.swing.JFormattedTextField();
        datePagamentos = new com.toedter.calendar.JDateChooser();
        jcmbFormaPagamento = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtableDividasPagamento = new javax.swing.JTable();
        btnPagar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtablePagamentos = new javax.swing.JTable();
        lblTaxas = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtPesquisarPagamento = new javax.swing.JTextField();

        setClosable(true);
        setMinimumSize(new java.awt.Dimension(577, 562));
        setNormalBounds(new java.awt.Rectangle(0, 0, 577, 562));
        setPreferredSize(new java.awt.Dimension(577, 562));
        getContentPane().setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 204, 0));
        jLabel2.setText("Abaixo a lista de todas as dividas QUE FORAM pagas!");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(140, 310, 310, 14);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 0, 255));
        jLabel1.setText("Abaixo a lista de todas as dividas NAO pagas!");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(140, 10, 280, 14);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Buscar : ");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 280, 70, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel5.setText("Valor Pago R$ :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(270, 180, 150, 20);

        txtValorDivida.setBorder(null);
        txtValorDivida.setForeground(new java.awt.Color(255, 0, 0));
        txtValorDivida.setDisabledTextColor(new java.awt.Color(222, 32, 11));
        txtValorDivida.setEnabled(false);
        txtValorDivida.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        txtValorDivida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtValorDividaActionPerformed(evt);
            }
        });
        getContentPane().add(txtValorDivida);
        txtValorDivida.setBounds(30, 200, 210, 20);

        txtValorPago.setBorder(null);
        txtValorPago.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        getContentPane().add(txtValorPago);
        txtValorPago.setBounds(270, 200, 220, 20);

        datePagamentos.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        getContentPane().add(datePagamentos);
        datePagamentos.setBounds(30, 250, 210, 28);
        datePagamentos.getDateEditor().addPropertyChangeListener(
            new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent e) {
                    if ("date".equals(e.getPropertyName())) {
                        clickDate();
                    }
                }
            });

            jcmbFormaPagamento.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
            jcmbFormaPagamento.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "À Vista (Dinheiro)", "Cartao Credito", "Cartao Debito", "Transferencia" }));
            jcmbFormaPagamento.setBorder(null);
            getContentPane().add(jcmbFormaPagamento);
            jcmbFormaPagamento.setBounds(270, 250, 220, 30);

            jLabel3.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
            jLabel3.setText("Forma de Pagamento :");
            getContentPane().add(jLabel3);
            jLabel3.setBounds(270, 230, 210, 20);

            jLabel6.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
            jLabel6.setText("Data : ");
            getContentPane().add(jLabel6);
            jLabel6.setBounds(30, 230, 150, 20);

            jtableDividasPagamento.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null}
                },
                new String [] {
                    "Title 1", "Title 2", "Title 3", "Title 4"
                }
            ));
            jtableDividasPagamento.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jtableDividasPagamentoMouseClicked(evt);
                }
            });
            jScrollPane2.setViewportView(jtableDividasPagamento);

            getContentPane().add(jScrollPane2);
            jScrollPane2.setBounds(30, 30, 490, 150);

            btnPagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_cash__25px.png"))); // NOI18N
            btnPagar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnPagarActionPerformed(evt);
                }
            });
            getContentPane().add(btnPagar);
            btnPagar.setBounds(510, 190, 40, 30);

            btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_delete_forever_25px.png"))); // NOI18N
            btnExcluir.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnExcluirActionPerformed(evt);
                }
            });
            getContentPane().add(btnExcluir);
            btnExcluir.setBounds(510, 250, 40, 30);

            jtablePagamentos.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null}
                },
                new String [] {
                    "Title 1", "Title 2", "Title 3", "Title 4"
                }
            ));
            jScrollPane1.setViewportView(jtablePagamentos);

            getContentPane().add(jScrollPane1);
            jScrollPane1.setBounds(10, 330, 540, 190);

            lblTaxas.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
            lblTaxas.setForeground(new java.awt.Color(255, 51, 51));
            lblTaxas.setText("+ Taxas");
            getContentPane().add(lblTaxas);
            lblTaxas.setBounds(100, 220, 180, 20);

            jLabel7.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
            jLabel7.setText("Valor Divida R$ :");
            getContentPane().add(jLabel7);
            jLabel7.setBounds(30, 180, 150, 20);

            txtPesquisarPagamento.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyTyped(java.awt.event.KeyEvent evt) {
                    txtPesquisarPagamentoKeyTyped(evt);
                }
            });
            getContentPane().add(txtPesquisarPagamento);
            txtPesquisarPagamento.setBounds(90, 280, 460, 25);

            setBounds(585, 20, 577, 562);
        }// </editor-fold>//GEN-END:initComponents

    public void clickDate() {
        JurosMulta jm = new JurosMulta();
        int linha = jtableDividasPagamento.getSelectedRow();
        //datePagamentos.setDate((Date) jtableDividasPagamento.getValueAt(linha, 2));
        String dataDivida = jtableDividasPagamento.getValueAt(linha, 2).toString();
        //String dataHoje = now.format(formatter);
        Date dataPagamento = datePagamentos.getDate();

        String valorDivida = jtableDividasPagamento.getValueAt(linha, 3).toString();
        //txtValorDivida.setForeground(Color.green);
        Date dataDividaFormatada = null;
        //Date dataHojeFormatada = null;
        Date dataPagamentoFormatada = null;

        try {
            dataDividaFormatada = formatterSimple.parse(dataDivida);
            //dataHojeFormatada = formatterSimple.parse(dataHoje);
            //dataPagamentoFormatada = formatterSimple.parse(dataPagamento);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this, "Nao foi possivel fazer a conversao das datas.\n\n" + ex, "CONVERSAO DATAS ERROR", JOptionPane.OK_OPTION);
        }
        txtValorDivida.setText(jtableDividasPagamento.getValueAt(linha, 3).toString());
        txtValorPago.setText(jtableDividasPagamento.getValueAt(linha, 3).toString());

        float valorDividaComparar = Float.parseFloat(jtableDividasPagamento.getValueAt(linha, 3).toString());

        //BigDecimal valorJurado = jm.calcularJuros(dataDividaFormatada, dataHojeFormatada, valorDivida);
        BigDecimal valorJurado = new BigDecimal(0);
        BigDecimal valorMultado = new BigDecimal(0);
        BigDecimal valorMultadoJurado = new BigDecimal(0);

        if (dataDividaFormatada.getDate() == dataPagamento.getDate()) {
            //System.out.println("teste");
            lblTaxas.setText("sem juros/multa (pago no dia)");
            lblTaxas.setForeground(new Color(9, 153, 3));
            lblTaxas.setVisible(true);
            txtValorDivida.setForeground(new Color(9, 153, 3));
        } else if (dataPagamento.getDate() > dataDividaFormatada.getDate()) {
            valorJurado = jm.calcularJuros(dataDividaFormatada, dataPagamento, valorDivida);
            valorMultado = jm.calcularMulta(valorDivida);
            valorMultadoJurado = valorJurado.add(valorMultado);
            float valorMJ = valorMultadoJurado.floatValue();
            //System.out.println("\nVALOR FINAL :  " + valorMultadoJurado);
            txtValorDivida.setText(String.valueOf(valorMultadoJurado));
            txtValorPago.setText(txtValorDivida.getText());
            if (valorDividaComparar < valorMJ) {
                txtValorDivida.setForeground(Color.red);
                lblTaxas.setText("multa 2% + juros 0.35% a/d");
                lblTaxas.setForeground(Color.red);
                lblTaxas.setVisible(true);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Data de pagamento tem que ser igual ou maior que a data da divida", "ERRO DE LOGICA", JOptionPane.WARNING_MESSAGE);
            try {
                datePagamentos.setDate(formatterSimple.parse(now.toString()));
            } catch (ParseException ex) {
                Logger.getLogger(CadastroPagamentos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private void txtValorDividaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtValorDividaActionPerformed

    }//GEN-LAST:event_txtValorDividaActionPerformed

    private void jtableDividasPagamentoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtableDividasPagamentoMouseClicked
        JurosMulta jm = new JurosMulta();
        int linha = jtableDividasPagamento.getSelectedRow();
        try {
            //datePagamentos.setDate((Date) jtableDividasPagamento.getValueAt(linha, 2));
            datePagamentos.setDate(formatterSimple.parse(now.toString()));
        } catch (ParseException ex) {
            Logger.getLogger(CadastroPagamentos.class.getName()).log(Level.SEVERE, null, ex);
        }

        String dataDivida = jtableDividasPagamento.getValueAt(linha, 2).toString();
        //String dataHoje = now.format(formatter);
        Date dataPagamento = datePagamentos.getDate();

        String valorDivida = jtableDividasPagamento.getValueAt(linha, 3).toString();
        Date dataDividaFormatada = null;
        //Date dataHojeFormatada = null;
        Date dataPagamentoFormatada = null;

        try {
            dataDividaFormatada = formatterSimple.parse(dataDivida);
            //dataHojeFormatada = formatterSimple.parse(dataHoje);
            //dataPagamentoFormatada = formatterSimple.parse(dataPagamento);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this, "Nao foi possivel fazer a conversao das datas.\n\n" + ex, "CONVERSAO DATAS ERROR", JOptionPane.OK_OPTION);
        }
        txtValorDivida.setText(jtableDividasPagamento.getValueAt(linha, 3).toString());
        txtValorPago.setText(jtableDividasPagamento.getValueAt(linha, 3).toString());

        float valorDividaComparar = Float.parseFloat(jtableDividasPagamento.getValueAt(linha, 3).toString());

        //BigDecimal valorJurado = jm.calcularJuros(dataDividaFormatada, dataHojeFormatada, valorDivida);
        BigDecimal valorJurado = new BigDecimal(0);
        BigDecimal valorMultado = new BigDecimal(0);
        BigDecimal valorMultadoJurado = new BigDecimal(0);

        if (dataDividaFormatada.getDate() == dataPagamento.getDate()) {
            System.out.println("teste");
            lblTaxas.setText("sem juros/multa (pago no dia)");
            lblTaxas.setForeground(new Color(9, 153, 3));
            lblTaxas.setVisible(true);
            txtValorDivida.setForeground(new Color(9, 153, 3));
        } else if (dataPagamento.getDate() > dataDividaFormatada.getDate()) {
            valorJurado = jm.calcularJuros(dataDividaFormatada, dataPagamento, valorDivida);
            valorMultado = jm.calcularMulta(valorDivida);
            valorMultadoJurado = valorJurado.add(valorMultado);
            float valorMJ = valorMultadoJurado.floatValue();
            System.out.println("\nVALOR FINAL :  " + valorMultadoJurado);
            txtValorDivida.setText(String.valueOf(valorMultadoJurado));
            txtValorPago.setText(txtValorDivida.getText());
            if (valorDividaComparar < valorMJ) {
                txtValorDivida.setForeground(Color.red);
                lblTaxas.setText("multa 2% + juros 0.35% a/d");
                lblTaxas.setForeground(Color.red);
                lblTaxas.setVisible(true);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Data de pagamento tem que ser igual ou maior que a data da divida", "ERRO DE LOGICA", JOptionPane.WARNING_MESSAGE);
            try {
                datePagamentos.setDate(formatterSimple.parse(now.toString()));
            } catch (ParseException ex) {
                Logger.getLogger(CadastroPagamentos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jtableDividasPagamentoMouseClicked

    public static int decisaoPagamentoMaior(String messagem) {
        int result = JOptionPane.showConfirmDialog((Component) null, messagem, "ALERTA", JOptionPane.OK_CANCEL_OPTION);
        return result;
    }

    public void preencherTablePagamentos(String sql) {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "pag_div_codigo", "Data", "Valor Pago", "Devedor", "FormaPagamento"};

        try {
            conex.getConnection();
            conex.executaQuerySQL(sql);
            conex.rs.first();

            do {
                dados.add(new Object[]{conex.rs.getInt("pag_codigo"), conex.rs.getInt("pag_div_codigo"), conex.rs.getDate("pag_datapagamento"), conex.rs.getBigDecimal("pag_valorpago"), conex.rs.getString("pe_nome"), conex.rs.getString("pag_forma")});
            } while (conex.rs.next());

            conex.rs.close();
        } catch (Exception e) {
            if (dados.size() < 1) {
                JOptionPane.showMessageDialog(this, "Nao existe dados na area de pagamentos", "SEM DADOS", JOptionPane.WARNING_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Erro ao preencher a tabela de pagamentos.\n\n" + e, "ERRO", JOptionPane.OK_OPTION);
            }
        }

        ModeloTabela modelo = new ModeloTabela(dados, colunas);
        jtablePagamentos.setModel(modelo);
        //pag_codigo
        jtablePagamentos.getColumnModel().getColumn(0).setPreferredWidth(30);
        jtablePagamentos.getColumnModel().getColumn(0).setResizable(false);
        //pag_div_codigo
        jtablePagamentos.getColumnModel().getColumn(1).setMaxWidth(0);
        jtablePagamentos.getColumnModel().getColumn(1).setMinWidth(0);
        jtablePagamentos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
        jtablePagamentos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);

        //pag_data pagamento
        jtablePagamentos.getColumnModel().getColumn(2).setPreferredWidth(80);
        jtablePagamentos.getColumnModel().getColumn(2).setResizable(false);
        //pag_valor pago
        jtablePagamentos.getColumnModel().getColumn(3).setPreferredWidth(70);
        jtablePagamentos.getColumnModel().getColumn(3).setResizable(false);
        //pe_nome devedor
        jtablePagamentos.getColumnModel().getColumn(4).setPreferredWidth(200);
        jtablePagamentos.getColumnModel().getColumn(4).setResizable(false);
        //pag_forma
        jtablePagamentos.getColumnModel().getColumn(5).setPreferredWidth(150);
        jtablePagamentos.getColumnModel().getColumn(5).setResizable(false);

        jtablePagamentos.getTableHeader().setReorderingAllowed(false);
        jtablePagamentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtablePagamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        conex.fecharConexao();

    }

    public void preencherTableDividas(String sql) {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "Devedor", "Data", "Valor R$", "Pago"};

        try {
            conex.getConnection();
            conex.executaQuerySQL(sql);
            conex.rs.first();

            do {
                dados.add(new Object[]{conex.rs.getInt("div_codigo"), conex.rs.getString("t2.pe_nome"), conex.rs.getDate("div_dataatualizacao"), conex.rs.getBigDecimal("div_valordadivida"), conex.rs.getString("div_pago")});
            } while (conex.rs.next());
            conex.rs.close();
        } catch (Exception e) {
            if (dados.size() < 1) {
                JOptionPane.showMessageDialog(this, "Nao existe dados na area de dividas", "SEM DADOS", JOptionPane.WARNING_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Erro ao preencher tabela de dividas", "TABELA DIVIDAS", JOptionPane.OK_OPTION);

            }
        }

        ModeloTabela modelo = new ModeloTabela(dados, colunas);
        jtableDividasPagamento.setModel(modelo);

        //ID
        jtableDividasPagamento.getColumnModel().getColumn(0).setPreferredWidth(25);
        jtableDividasPagamento.getColumnModel().getColumn(0).setResizable(false);
        //Devedor
        jtableDividasPagamento.getColumnModel().getColumn(1).setPreferredWidth(250);
        jtableDividasPagamento.getColumnModel().getColumn(1).setResizable(true);
        //Data
        jtableDividasPagamento.getColumnModel().getColumn(2).setPreferredWidth(80);
        jtableDividasPagamento.getColumnModel().getColumn(2).setResizable(true);
        //Valor da Divida
        jtableDividasPagamento.getColumnModel().getColumn(3).setPreferredWidth(68);
        jtableDividasPagamento.getColumnModel().getColumn(3).setResizable(true);
        //Pago
        jtableDividasPagamento.getColumnModel().getColumn(4).setPreferredWidth(50);
        jtableDividasPagamento.getColumnModel().getColumn(4).setResizable(true);

        jtableDividasPagamento.getTableHeader().setReorderingAllowed(false);
        jtableDividasPagamento.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jtableDividasPagamento.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        conex.fecharConexao();
    }

    private void btnPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagarActionPerformed
        Alterar alterar = new Alterar();
        int linha = jtableDividasPagamento.getSelectedRow();
        int id = Integer.parseInt(jtableDividasPagamento.getValueAt(linha, 0).toString());
        if (txtValorPago.getText().isEmpty() || datePagamentos.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Existem campos nao preenchidos", "CAMPOS VAZIOS", JOptionPane.WARNING_MESSAGE);
        } else if (Float.parseFloat(txtValorPago.getText()) < Float.parseFloat(txtValorDivida.getText())) {
            JOptionPane.showMessageDialog(this, "O Valor do pagamento e menor que o valor da divida", "Valor Incorreto", JOptionPane.OK_OPTION);
        } else if (Float.parseFloat(txtValorPago.getText()) > Float.parseFloat(txtValorDivida.getText())) {
            int retorno = decisaoPagamentoMaior("Voce esta pagando um valor maior que a Divida, tem certeza?");
            //retorno == 0 apertou OK
            if (retorno == 0) {
                linha = jtableDividasPagamento.getSelectedRow();
                //id = Integer.parseInt(jtableDividasPagamento.getValueAt(linha, 0).toString());
                //String pagante = jtableDividasPagamento.getValueAt(linha, 1).toString();
                Cadastrar cadastro = new Cadastrar();
                try {
                    cadastro.cadastrarPagamento(id, datePagamentos.getDate(), Float.parseFloat(txtValorPago.getText()), jcmbFormaPagamento.getSelectedItem().toString());
                    alterar.alterarStatusDivida(id, "SIM");
                    preencherTableDividas(dividasQuerySelect);
                    preencherTablePagamentos(pagamentosQuerySelect);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(this, "Erro ao pegar a divida" + ex, "ERRO", JOptionPane.OK_OPTION);
                }
            }
        } else {
            linha = jtableDividasPagamento.getSelectedRow();
            //id = Integer.parseInt(jtableDividasPagamento.getValueAt(linha, 0).toString());
            //String pagante = jtableDividasPagamento.getValueAt(linha, 1).toString();
            Cadastrar cadastro = new Cadastrar();
            try {
                cadastro.cadastrarPagamento(id, datePagamentos.getDate(), Float.parseFloat(txtValorPago.getText()), jcmbFormaPagamento.getSelectedItem().toString());
                alterar.alterarStatusDivida(id, "SIM");
                preencherTableDividas(dividasQuerySelect);
                preencherTablePagamentos(pagamentosQuerySelect);

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Erro ao pegar a divida" + ex, "ERRO", JOptionPane.OK_OPTION);
            }
        }
    }//GEN-LAST:event_btnPagarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed

        Deletar delete = new Deletar();
        Alterar alterar = new Alterar();
        int linha = jtablePagamentos.getSelectedRow();
        int idpag = Integer.parseInt(jtablePagamentos.getValueAt(linha, 0).toString());
        int idDivida = pegarDivIDpag(idpag);
        try {
            alterar.alterarStatusDivida(idDivida, "NAO");
            delete.deletarPagamento(idpag);
            preencherTableDividas(dividasQuerySelect);
            preencherTablePagamentos(pagamentosQuerySelect);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Erro ao excluir o pagamento.\n\n" + ex, "EXCLUIR PAGAMENTO", JOptionPane.OK_OPTION);
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void txtPesquisarPagamentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisarPagamentoKeyTyped

        //String caracteres = "0987654321";
        if (evt.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
            if (txtPesquisarPagamento.getText().length() < 1) {
                preencherTablePagamentos(pagamentosQuerySelect);
            } else {
                Pesquisar pesquisar = new Pesquisar();
                pesquisar.pesquisarPagamentoCPF("SELECT pag_codigo, pag_div_codigo, pag_datapagamento, pag_valorpago, pag_forma, div_codigo, div_devedor, pe_codigo, pe_nome, pe_documento \n"
                        + "FROM tb_pagamento \n"
                        + "INNER JOIN tb_divida ON tb_pagamento.pag_div_codigo = tb_divida.div_codigo\n"
                        + "INNER JOIN tb_pessoa ON tb_divida.div_devedor = tb_pessoa.pe_codigo \n"
                        + "where pe_documento LIKE '%" + txtPesquisarPagamento.getText() + "%'");
            }
        } else {
            Pesquisar pesquisar = new Pesquisar();
            pesquisar.pesquisarPagamentoCPF("SELECT pag_codigo, pag_div_codigo, pag_datapagamento, pag_valorpago, pag_forma, div_codigo, div_devedor, pe_codigo, pe_nome, pe_documento \n"
                    + "FROM tb_pagamento \n"
                    + "INNER JOIN tb_divida ON tb_pagamento.pag_div_codigo = tb_divida.div_codigo\n"
                    + "INNER JOIN tb_pessoa ON tb_divida.div_devedor = tb_pessoa.pe_codigo \n"
                    + "where pe_documento LIKE '%" + txtPesquisarPagamento.getText() + "%'");
        }
    }//GEN-LAST:event_txtPesquisarPagamentoKeyTyped

    public int pegarDivIDpag(int idpag) {
        int idDiv = 0;
        Connection conn = conex.getConnection();
        ResultSet rs = null;
        try {
            PreparedStatement create = conn.prepareStatement("SELECT pag_div_codigo from tb_pagamento where pag_codigo = ?");
            create.setInt(1, idpag);
            rs = create.executeQuery();
            //int idDiv
            rs.first();

            idDiv = Integer.parseInt(rs.getObject(1).toString());
            //System.out.println("VALOR: " + idDiv);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Erro ao pegar ID da Divida no Pagamento.\n\n" + ex, "ERRO", JOptionPane.OK_OPTION);
        }

        return idDiv;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPagar;
    private com.toedter.calendar.JDateChooser datePagamentos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox jcmbFormaPagamento;
    private javax.swing.JTable jtableDividasPagamento;
    public static javax.swing.JTable jtablePagamentos;
    private javax.swing.JLabel lblTaxas;
    private javax.swing.JTextField txtPesquisarPagamento;
    private javax.swing.JFormattedTextField txtValorDivida;
    private javax.swing.JFormattedTextField txtValorPago;
    // End of variables declaration//GEN-END:variables

}
