/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package design;

import java.awt.Color;
import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import mysqlConexao.Conexao;

/**
 *
 * @author adria
 */
public class Login extends javax.swing.JFrame {

    Conexao conex = new Conexao();

    /**
     * Creates new form Login
     */
    public Login() {
        initComponents();
        txtNomeCadastro.setForeground(Color.gray);
        txtNomeCadastro.setFont(new Font("Tahoma", Font.ITALIC, 14));
        txtNomeCadastro.setText("Nome");

        txtCargoCadastro.setForeground(Color.gray);
        txtCargoCadastro.setFont(new Font("Tahoma", Font.ITALIC, 14));
        txtCargoCadastro.setText("Cargo");

        txtUsuarioCadastro.setForeground(Color.gray);
        txtUsuarioCadastro.setFont(new Font("Tahoma", Font.ITALIC, 14));
        txtUsuarioCadastro.setText("Usuario");

        txtSenhaCadastro.setForeground(Color.gray);
        txtSenhaCadastro.setFont(new Font("Tahoma", Font.ITALIC, 14));
        txtSenhaCadastro.setText("Senha");

        txtEmailCadastro.setForeground(Color.gray);
        txtEmailCadastro.setFont(new Font("Tahoma", Font.ITALIC, 14));
        txtEmailCadastro.setText("E-Mail");

        txtUsuarioLogin.setForeground(Color.gray);
        txtUsuarioLogin.setFont(new Font("Tahoma", Font.ITALIC, 14));
        txtUsuarioLogin.setText("Usuario");

        txtSenhaLogin.setForeground(Color.gray);
        txtSenhaLogin.setFont(new Font("Tahoma", Font.ITALIC, 14));
        txtSenhaLogin.setText("Senha");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtUsuarioLogin = new javax.swing.JTextField();
        btnLogin = new javax.swing.JButton();
        txtSenhaLogin = new javax.swing.JPasswordField();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtEmailCadastro = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnCadastro = new javax.swing.JButton();
        txtNomeCadastro = new javax.swing.JTextField();
        txtUsuarioCadastro = new javax.swing.JTextField();
        txtSenhaCadastro = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtCargoCadastro = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(638, 413));
        setPreferredSize(new java.awt.Dimension(638, 413));
        setSize(new java.awt.Dimension(638, 413));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_login_rounded_right_60px.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 45, -1, 50));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_key_60px.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 60, -1));

        txtUsuarioLogin.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        getContentPane().add(txtUsuarioLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 50, 200, 40));

        btnLogin.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_under_computer_20px.png"))); // NOI18N
        btnLogin.setText("LOGIN");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 180, 110, 30));

        txtSenhaLogin.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        getContentPane().add(txtSenhaLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 200, 40));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(240, 240, 240));
        jPanel1.setMinimumSize(new java.awt.Dimension(638, 312));
        jPanel1.setPreferredSize(new java.awt.Dimension(638, 312));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(153, 0, 153));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, 10, 350));

        txtEmailCadastro.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        jPanel1.add(txtEmailCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 210, 200, -1));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/loginIcones/icons8_login_rounded_right_30px.png"))); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 130, 30, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/loginIcones/icons8_user_30px.png"))); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 50, -1, 30));

        btnCadastro.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCadastro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_under_computer_20px.png"))); // NOI18N
        btnCadastro.setText("CADASTRO");
        btnCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastroActionPerformed(evt);
            }
        });
        jPanel1.add(btnCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 270, 140, 30));

        txtNomeCadastro.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        jPanel1.add(txtNomeCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 50, 200, -1));

        txtUsuarioCadastro.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        jPanel1.add(txtUsuarioCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 130, 200, -1));

        txtSenhaCadastro.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        jPanel1.add(txtSenhaCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 170, 200, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/loginIcones/icons8_key_30px.png"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, 30, -1));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/loginIcones/icons8_permanent_job_30px.png"))); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 90, 30, -1));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/loginIcones/icons8_email_30px.png"))); // NOI18N
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 30, -1));

        txtCargoCadastro.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        jPanel1.add(txtCargoCadastro, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 90, 200, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 640, 410));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastroActionPerformed

        Connection conn;
        ResultSet rs;

        try {
            conn = conex.getConnection();

            PreparedStatement create = conn.prepareStatement("insert into tb_usuario (user_nome, user_cargo, user_login, user_senha, user_email) VALUES (?, ?, ?, ?, ?)");
            create.setString(1, txtNomeCadastro.getText());
            create.setString(2, txtCargoCadastro.getText());
            create.setString(3, txtUsuarioCadastro.getText());
            create.setString(4, txtSenhaCadastro.getText());
            create.setString(5, txtEmailCadastro.getText());

            create.executeUpdate();
            create.close();
            conn.close();
            JOptionPane.showMessageDialog(this, "Novo usuario cadastrado com sucesso!\n\n", "Sucesso!!", JOptionPane.OK_OPTION);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, "ERRO\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCadastroActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        String user = txtUsuarioLogin.getText();
        String pass = String.valueOf(txtSenhaLogin.getPassword());

        Connection conn = conex.getConnection();
        ResultSet rs = null;
        try {
            PreparedStatement create = conn.prepareStatement("select * from tb_usuario where user_login = ? and user_senha = ?");
            
            create.setString(1, user);
            create.setString(2, pass);
            
            rs = create.executeQuery();
            
            if(rs.next()){
                CadastroClientes cadCli = new CadastroClientes();
                cadCli.setVisible(true);
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "Login ou senha incorretos!", "Login Erro", JOptionPane.WARNING_MESSAGE);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, "Falha ao conectar com o banco!\n\n"+e, "DB Connect", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnLoginActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCadastro;
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtCargoCadastro;
    private javax.swing.JTextField txtEmailCadastro;
    private javax.swing.JTextField txtNomeCadastro;
    private javax.swing.JTextField txtSenhaCadastro;
    private javax.swing.JPasswordField txtSenhaLogin;
    private javax.swing.JTextField txtUsuarioCadastro;
    private javax.swing.JTextField txtUsuarioLogin;
    // End of variables declaration//GEN-END:variables
}
