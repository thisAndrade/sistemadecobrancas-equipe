package design;

import clienteDividasPagamentos.Atualizar;
import clienteDividasPagamentos.Cadastrar;
import clienteDividasPagamentos.Deletar;
import clienteDividasPagamentos.Pesquisar;
import static design.CadastroClientes.decisaoDeletar;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.sql.*;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import model.ModeloTabela;
import mysqlConexao.Conexao;

public class CadastroDividas extends javax.swing.JInternalFrame {

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    Conexao conex = new Conexao();
    public String selectTodasDividas = "select div_codigo, div_credor, div_devedor, div_dataatualizacao, div_valordadivida, div_pago, t1.pe_nome, t2.pe_nome "
            + "from tb_divida inner join tb_pessoa t1 on tb_divida.div_credor = t1.pe_codigo "
            + "inner join tb_pessoa t2 on tb_divida.div_devedor = t2.pe_codigo where div_pago = 'NAO'";
    public String sqlComboBox = "select * from tb_pessoa";

    public CadastroDividas() {
        initComponents();
        preencherTabelaDividas(selectTodasDividas);
        preencherComboBoxDevedor(sqlComboBox);
        preencherComboBoxCredor(sqlComboBox);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jcmbCredor = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jcmbDevedor = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        checkHoje = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        btnCadastrarDivida = new javax.swing.JButton();
        btnAlterarDivida = new javax.swing.JButton();
        btnExcluirDivida = new javax.swing.JButton();
        txtPesquisarDividaCPF = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtableDividas = new javax.swing.JTable();
        dateData = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();

        setClosable(true);
        setMinimumSize(new java.awt.Dimension(583, 354));
        setNormalBounds(new java.awt.Rectangle(0, 30, 576, 326));
        setPreferredSize(new java.awt.Dimension(576, 326));
        getContentPane().setLayout(null);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel4.setText("Credor : ");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 10, 100, 20);

        jcmbCredor.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jcmbCredor.setForeground(new java.awt.Color(0, 153, 51));
        jcmbCredor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-- Item 1", "Item 2", "Item 3", "Item 4" }));
        jcmbCredor.setBorder(null);
        jcmbCredor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcmbCredorActionPerformed(evt);
            }
        });
        getContentPane().add(jcmbCredor);
        jcmbCredor.setBounds(30, 30, 219, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel2.setText("Devedor :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(270, 10, 100, 20);

        jcmbDevedor.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jcmbDevedor.setForeground(new java.awt.Color(0, 153, 51));
        jcmbDevedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-- Item 1", "Item 2", "Item 3", "Item 4" }));
        jcmbDevedor.setBorder(null);
        jcmbDevedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jcmbDevedorMouseClicked(evt);
            }
        });
        getContentPane().add(jcmbDevedor);
        jcmbDevedor.setBounds(270, 30, 220, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel3.setText("Valor R$ :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(270, 65, 100, 20);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 51, 51));
        jLabel5.setText("HOJE");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(190, 63, 50, 20);

        checkHoje.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        checkHoje.setForeground(new java.awt.Color(0, 153, 51));
        checkHoje.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkHojeMouseClicked(evt);
            }
        });
        getContentPane().add(checkHoje);
        checkHoje.setBounds(234, 63, 21, 21);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel7.setText("Data :");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(30, 65, 100, 20);

        txtValor.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        txtValor.setForeground(new java.awt.Color(255, 0, 0));
        txtValor.setBorder(null);
        txtValor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtValorActionPerformed(evt);
            }
        });
        txtValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValorKeyTyped(evt);
            }
        });
        getContentPane().add(txtValor);
        txtValor.setBounds(270, 82, 220, 30);

        btnCadastrarDivida.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCadastrarDivida.setForeground(new java.awt.Color(0, 153, 51));
        btnCadastrarDivida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_user_male_25px_1.png"))); // NOI18N
        btnCadastrarDivida.setBorder(null);
        btnCadastrarDivida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarDividaActionPerformed(evt);
            }
        });
        getContentPane().add(btnCadastrarDivida);
        btnCadastrarDivida.setBounds(508, 20, 40, 30);

        btnAlterarDivida.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnAlterarDivida.setForeground(new java.awt.Color(0, 153, 51));
        btnAlterarDivida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_save_25px.png"))); // NOI18N
        btnAlterarDivida.setBorder(null);
        btnAlterarDivida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarDividaActionPerformed(evt);
            }
        });
        getContentPane().add(btnAlterarDivida);
        btnAlterarDivida.setBounds(508, 59, 40, 30);

        btnExcluirDivida.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnExcluirDivida.setForeground(new java.awt.Color(0, 153, 51));
        btnExcluirDivida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_delete_forever_25px.png"))); // NOI18N
        btnExcluirDivida.setBorder(null);
        btnExcluirDivida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirDividaActionPerformed(evt);
            }
        });
        getContentPane().add(btnExcluirDivida);
        btnExcluirDivida.setBounds(508, 98, 40, 30);

        txtPesquisarDividaCPF.setBorder(null);
        txtPesquisarDividaCPF.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPesquisarDividaCPFKeyTyped(evt);
            }
        });
        getContentPane().add(txtPesquisarDividaCPF);
        txtPesquisarDividaCPF.setBounds(110, 119, 380, 22);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Busca(CPF) :");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(30, 119, 80, 20);

        jtableDividas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtableDividas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtableDividasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtableDividas);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(10, 170, 540, 120);
        getContentPane().add(dateData);
        dateData.setBounds(30, 82, 220, 30);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 0, 255));
        jLabel1.setText("Abaixo a lista de todas as dividas NAO pagas!");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(140, 150, 280, 14);

        setBounds(0, 20, 576, 326);
    }// </editor-fold>//GEN-END:initComponents

    private void jcmbCredorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcmbCredorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcmbCredorActionPerformed

    private void jcmbDevedorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jcmbDevedorMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jcmbDevedorMouseClicked

    public void preencherComboBoxCredor(String Sql) {
        ArrayList<String> strList = new ArrayList<String>();
        try {
            conex.getConnection();
            conex.executaQuerySQL(Sql);
            conex.rs.first();

            do {
                strList.add(conex.rs.getString("pe_nome"));
            } while (conex.rs.next());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher a caixa de selecao", "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        DefaultComboBoxModel defaultComboBox = new DefaultComboBoxModel(strList.toArray());
        jcmbCredor.setModel(defaultComboBox);

    }

    public void preencherComboBoxDevedor(String Sql) {
        ArrayList<String> strList = new ArrayList<String>();
        try {
            conex.getConnection();
            conex.executaQuerySQL(Sql);
            conex.rs.first();

            do {
                strList.add(conex.rs.getString("pe_nome"));
            } while (conex.rs.next());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher a caixa de selecao", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        DefaultComboBoxModel defaultComboBox = new DefaultComboBoxModel(strList.toArray());
        jcmbDevedor.setModel(defaultComboBox);

    }

    private void checkHojeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkHojeMouseClicked
        // TODO add your handling code here:
        if (checkHoje.isSelected()) {
            //logica da data aqui
            setaDataAtual();
        } else {
            dateData.setDate(null);
        }
    }//GEN-LAST:event_checkHojeMouseClicked

    private void setaDataAtual() {
        java.util.Date hoje = new java.util.Date();
        dateData.setDate(hoje);
    }

    private void txtValorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtValorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtValorActionPerformed

    private void btnCadastrarDividaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarDividaActionPerformed

        if (!txtValor.getText().isEmpty() && dateData.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Voce precisa colocar uma data", "DATA VAZIA", JOptionPane.WARNING_MESSAGE);
        } else if (txtValor.getText().isEmpty() && dateData.getDate() != null) {
            JOptionPane.showMessageDialog(this, "Voce precisa colocar um valor na Divida", "VALOR VAZIO", JOptionPane.WARNING_MESSAGE);
        } else if (txtValor.getText().isEmpty() && dateData.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Campos de Data e Valor estao vazios favor preencher!!!", "CAMPOS VAZIOS", JOptionPane.WARNING_MESSAGE);
        } else if (jcmbCredor.getSelectedItem().equals(jcmbDevedor.getSelectedItem())) {
            JOptionPane.showMessageDialog(this, "O Credor nao pode ser igual ao Devedor", "CREDOR IGUAL AO DEVEDOR", JOptionPane.WARNING_MESSAGE);
        } else {
            //String cpfDevedor = pegarCPF(jcmbDevedor.getSelectedItem().toString());
            Cadastrar cadastrar = new Cadastrar();
            Pesquisar pesquisar = new Pesquisar();
            try {
                Date data = dateData.getDate();
                int idCredor = pesquisar.pegarIDpessoa(jcmbCredor.getSelectedItem().toString());
                int idDevedor = pesquisar.pegarIDpessoa(jcmbDevedor.getSelectedItem().toString());
                cadastrar.cadastrarDivida(idCredor, idDevedor, data, Float.valueOf(txtValor.getText()), "NAO");
                preencherTabelaDividas(selectTodasDividas);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Nao foi possivel inserir a DIVIDA\n\n" + e, "ERROR DIVIDA", JOptionPane.OK_OPTION);
            }
        }
    }//GEN-LAST:event_btnCadastrarDividaActionPerformed

    private String pegarCPF(String nome) {
        Connection conn = conex.getConnection();
        ResultSet rs = null;
        String cpfRecebido = null;
        try {
            PreparedStatement create = conn.prepareStatement("SELECT cpf FROM cliente WHERE nomeCliente =  ? ");
            create.setString(1, nome);
            rs = create.executeQuery();
            rs.first();
            //if (rs.next()) {
            String i = rs.getObject(1).toString();
            cpfRecebido = i;
            //System.out.println(cpfRecebido);
            //System.out.println(i);
            //}
            conn.close();
            create.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Nao foi possivel pegar o CPF", "ERROR CPF", JOptionPane.OK_OPTION);
        }
        return cpfRecebido;
    }

    private void btnAlterarDividaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarDividaActionPerformed
        if (!txtValor.getText().isEmpty() && dateData.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Voce precisa colocar uma data", "DATA VAZIA", JOptionPane.WARNING_MESSAGE);
        } else if (txtValor.getText().isEmpty() && dateData.getDate() != null) {
            JOptionPane.showMessageDialog(this, "Voce precisa colocar um valor na Divida", "VALOR VAZIO", JOptionPane.WARNING_MESSAGE);
        } else if (txtValor.getText().isEmpty() && dateData.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Campos de Data e Valor estao vazios favor preencher!!!", "CAMPOS VAZIOS", JOptionPane.WARNING_MESSAGE);
        } else if (jcmbCredor.getSelectedItem().equals(jcmbDevedor.getSelectedItem())) {
            JOptionPane.showMessageDialog(this, "O Credor nao nao pode ser igual ao Devedor", "CREDOR IGUAL AO DEVEDOR", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                Atualizar atualizar = new Atualizar();
                Pesquisar pesquisar = new Pesquisar();

                int linha = jtableDividas.getSelectedRow();
                int codigo = Integer.parseInt(jtableDividas.getValueAt(linha, 0).toString());
                int idCredor = pesquisar.pegarIDpessoa(jcmbCredor.getSelectedItem().toString());
                int idDevedor = pesquisar.pegarIDpessoa(jcmbDevedor.getSelectedItem().toString());

                atualizar.atualizarDivida(codigo, idCredor, idDevedor, dateData.getDate(), Float.parseFloat(txtValor.getText()));
                preencherTabelaDividas(selectTodasDividas);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Erro ao alterar a Divida, feche o programa e tente novamente\n\n" + ex, "ERRO NA ALTERACAO", JOptionPane.OK_OPTION);
            }
        }
    }//GEN-LAST:event_btnAlterarDividaActionPerformed

    public void preencherTabelaDividas(String Sql) {
        //Crio uma nova Lista de dados chamada "dados"
        ArrayList dados = new ArrayList();
        //Crio um vetor com os nomes das colunas do jTable.
        String[] colunas = new String[]{"ID", "Credor", "Devedor", "Data", "Valor R$", "PAGO", "div_credor", "div_devedor"};

        //Tratamento de erro para conexao
        try {
            //Abro a Conexao para executar os comandos dentro do banco.
            conex.getConnection();

            //Executo a query que chamei no carregamento do Form "select * from cliente".
            conex.executaQuerySQL(Sql);

            //Vai para o primeiro objeto, a primeira linha do "select * from cliente".
            conex.rs.first();
            do {
                //Adiciona conforme vai passando as linhas para o ArrayList criado "dados".
                dados.add(new Object[]{conex.rs.getInt("div_codigo"), conex.rs.getString("t1.pe_nome"), conex.rs.getString("t2.pe_nome"), conex.rs.getDate("div_dataatualizacao"), conex.rs.getBigDecimal("div_valordadivida"), conex.rs.getString("div_pago"), conex.rs.getInt("div_credor"), conex.rs.getInt("div_devedor")});
            } while (conex.rs.next());//enquanto tiver valor na proxima linha ele vai ficar fazendo isso
            conex.stm.close();
            conex.rs.close();
        } catch (SQLException e) {
            //Exibe uma caixa de Mensagem com erro
            try {
                if (dados.size() < 1) {
                    JOptionPane.showMessageDialog(this, "Nao existe dados na area de dividas", "SEM DADOS", JOptionPane.WARNING_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Erro ao preencher a lista, verifique se o banco de dados esta online\n\n" + e, "ERRO", JOptionPane.OK_OPTION);
                }
            } catch (Exception s) {
            }
        }
        //Crio uma instancia da classe ModeloTabela e inicializo o metodo construtor
        //ja com os dados (as linhas, os objetos) que foram adicionados no "while"
        //e as colunas, os nomes das colunas que foram armazenadas no vetor de string.
        ModeloTabela modelo = new ModeloTabela(dados, colunas);

        //Defino o modelo de tabela para o meu jtableDividas, ou seja, ele vai seguir 
        //os metodos escritos na minha classe ModeloTabela.
        jtableDividas.setModel(modelo);

        //Aqui em baixo eu so atribuo o tamanho de cada coluna de acordo com a index
        //indo de 0 = primeira coluna, ate 7 = minha ultima coluna.
        //Codigo
        jtableDividas.getColumnModel().getColumn(0).setPreferredWidth(30);
        jtableDividas.getColumnModel().getColumn(0).setResizable(true); //nao permito que a primeira coluna tenha o tamanho modificado
        //Credor
        jtableDividas.getColumnModel().getColumn(1).setPreferredWidth(160);
        jtableDividas.getColumnModel().getColumn(1).setResizable(true); //as seguintes eu permito isso.
        //Devedor
        jtableDividas.getColumnModel().getColumn(2).setPreferredWidth(160);
        jtableDividas.getColumnModel().getColumn(2).setResizable(true);
        //Data Atualizacao
        jtableDividas.getColumnModel().getColumn(3).setPreferredWidth(80);
        jtableDividas.getColumnModel().getColumn(3).setResizable(true);
        //Valor da Divida
        jtableDividas.getColumnModel().getColumn(4).setPreferredWidth(60);
        jtableDividas.getColumnModel().getColumn(4).setResizable(true);
        //Pago
        jtableDividas.getColumnModel().getColumn(5).setPreferredWidth(50);
        jtableDividas.getColumnModel().getColumn(5).setResizable(true);

        //div_credor - deixar invisivel
        jtableDividas.getColumnModel().getColumn(6).setMaxWidth(0);
        jtableDividas.getColumnModel().getColumn(6).setMinWidth(0);
        jtableDividas.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
        jtableDividas.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);

        //div_devedor - deixar invisivel
        jtableDividas.getColumnModel().getColumn(7).setMaxWidth(0);
        jtableDividas.getColumnModel().getColumn(7).setMinWidth(0);
        jtableDividas.getTableHeader().getColumnModel().getColumn(7).setMaxWidth(0);
        jtableDividas.getTableHeader().getColumnModel().getColumn(7).setMinWidth(0);

        //nao pode alterar a ordem
        jtableDividas.getTableHeader().setReorderingAllowed(false);
        //nao pode ser redimensionada automaticamente.
        jtableDividas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Selecionar apenas um dado na tabela por vez
        jtableDividas.setSelectionMode((ListSelectionModel.SINGLE_SELECTION));

        conex.fecharConexao(); //fecho a conexao depois de tudo.
    }

    private void btnExcluirDividaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirDividaActionPerformed

        int linha = jtableDividas.getSelectedRow();
        int retorno = decisaoDeletar("Tem certeza que deseja apagar essa divida?");
        if (retorno == 0) {
            int id = Integer.parseInt(jtableDividas.getValueAt(linha, 0).toString());
            Deletar delete = new Deletar();
            try {
                delete.deletarDivida(id);
                preencherTabelaDividas(selectTodasDividas);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Erro ao apagar a Divida.\n\n" + ex, "ERRO AO APAGAR", JOptionPane.ERROR_MESSAGE);
            }
        } else {

        }
    }//GEN-LAST:event_btnExcluirDividaActionPerformed

    public int verificarDividas(int idDevedor) {
        Connection conn = null;
        ResultSet rs = null;
        int quantiaDividas = 0;
        try {
            conn = conex.getConnection();
            PreparedStatement create = conn.prepareStatement("SELECT COUNT(*) FROM tb_divida where div_devedor = ?");
            create.setInt(1, idDevedor);
            rs = create.executeQuery();
            rs.first();

            quantiaDividas = Integer.parseInt(rs.getObject(1).toString());
        } catch (SQLException | NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Erro ao verificar as Dividas.\n\n" + e, "ERRO AO VERIFICAR", JOptionPane.ERROR_MESSAGE);
        }

        return quantiaDividas;
    }

    private void jtableDividasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtableDividasMouseClicked
        int linha = jtableDividas.getSelectedRow();
        jcmbCredor.setSelectedItem(jtableDividas.getValueAt(linha, 1));
        txtValor.setText(jtableDividas.getValueAt(linha, 4).toString());
        jcmbDevedor.setSelectedItem(jtableDividas.getValueAt(linha, 2));
    }//GEN-LAST:event_jtableDividasMouseClicked

    private void txtValorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorKeyTyped
        String caracteres = "0987654321";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtValorKeyTyped

    private void txtPesquisarDividaCPFKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisarDividaCPFKeyTyped
        //String caracteres = "0987654321";
        if (evt.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
            if (txtPesquisarDividaCPF.getText().length() < 1) {
                preencherTabelaDividas(selectTodasDividas);
            } else {
                Pesquisar pesquisar = new Pesquisar();
                pesquisar.pesquisarDividaCPF("SELECT div_codigo, div_credor, div_devedor, div_dataatualizacao, div_valordadivida, div_pago, pe_documento, pe_nome\n"
                        + "FROM tb_divida\n"
                        + "INNER JOIN tb_pessoa ON tb_divida.div_devedor = tb_pessoa.pe_codigo\n"
                        + "where pe_documento LIKE '%" + txtPesquisarDividaCPF.getText() + "%'");
            }
        } else {
            Pesquisar pesquisar = new Pesquisar();
            pesquisar.pesquisarDividaCPF("SELECT div_codigo, div_credor, div_devedor, div_dataatualizacao, div_valordadivida, div_pago, pe_documento, pe_nome\n"
                    + "FROM tb_divida\n"
                    + "INNER JOIN tb_pessoa ON tb_divida.div_devedor = tb_pessoa.pe_codigo\n"
                    + "where pe_documento LIKE '%" + txtPesquisarDividaCPF.getText() + "%'");

        }
    }//GEN-LAST:event_txtPesquisarDividaCPFKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterarDivida;
    private javax.swing.JButton btnCadastrarDivida;
    private javax.swing.JButton btnExcluirDivida;
    private javax.swing.JCheckBox checkHoje;
    private com.toedter.calendar.JDateChooser dateData;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox jcmbCredor;
    private javax.swing.JComboBox jcmbDevedor;
    public static javax.swing.JTable jtableDividas;
    private javax.swing.JTextField txtPesquisarDividaCPF;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
